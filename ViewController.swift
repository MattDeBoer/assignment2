//
//  ViewController.swift
//  Assignment2
//
//  Created by Matt DeBoer on 1/17/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
        var sv : UIScrollView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sv = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        sv.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin]
        self.view.addSubview(sv)
        
        
        sv.contentSize = CGSize(width: self.view.frame.size.width*3, height: self.view.frame.size.height*3)
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view.backgroundColor = .blue
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin]
        sv.addSubview(view)
        
        let view1 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view1.backgroundColor = .yellow
        view1.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleTopMargin]
        sv.addSubview(view1)
        
        let view2 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height*2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view2.backgroundColor = .orange
        view2.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleRightMargin, .flexibleBottomMargin, .flexibleTopMargin]
        sv.addSubview(view2)
        
        let view3 = UIView(frame: CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view3.backgroundColor = .red
        view3.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin]
        sv.addSubview(view3)
        
        let view4 = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view4.backgroundColor = .green
        view4.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin]
        sv.addSubview(view4)
        
        let view5 = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height*2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view5.backgroundColor = .purple
        view5.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin,. flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        sv.addSubview(view5)
        
        let view6 = UIView(frame: CGRect(x: self.view.frame.size.width*2, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view6.backgroundColor = .gray
        view6.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
        sv.addSubview(view6)
        
        let view7 = UIView(frame: CGRect(x: self.view.frame.size.width*2, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view7.backgroundColor = .magenta
        view7.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
        sv.addSubview(view7)
        
        let view8 = UIView(frame: CGRect(x: self.view.frame.size.width*2, y: self.view.frame.size.height*2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view8.backgroundColor = .white
        view8.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        sv.addSubview(view8)
      
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        sv.contentSize = CGSize(width: size.width*3, height: size.height*3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

